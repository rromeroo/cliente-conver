import { Component, OnInit, Input } from '@angular/core';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-conversor',
  templateUrl: './conversor.component.html',
  styleUrls: ['./conversor.component.css']
})
export class ConversorComponent implements OnInit {

  datoSend = { unidad_dato: "", unidad_conversion:"", dato:""};
  datoReceived: any;

  constructor(public rest: RestService, private route: ActivatedRoute, private Router: Router){}

  ngOnInit() {
  }

  convertir() {
    this.rest.doconversion(this.datoSend).subscribe((result) => {
      
      this.datoReceived = result;
      console.log(this.datoReceived);
    },(err) => {
      console.log(err);
    });
  }

  /*limpiar(){
    this.resultado.suma = 0;
    this.numeros.datoa = "";
    this.numeros.datob = "";
  }
  */
}

